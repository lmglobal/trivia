'use strict';
var mysql = require('mysql');
let responseMsg;
exports.getMonthProgress =  (event, context, callback) => {
  /*var pool  = mysql.createPool({
    host     : '192.168.1.11',
    user     : 'sriAWS',
    password : 'admin',
    database : 'smahtest'
  });*/

  
  var pool  = mysql.createPool({
    host     : 'smahtest.c1gggoku9c0l.us-east-1.rds.amazonaws.com',
    user     : 'admin',
    password : 'Smahtest20$',
    database : 'smahtest'
  });
  
  var AES = require("crypto-js/aes");
  var Utf8 = require("crypto-js/enc-utf8");  
  var secretKeyToDecrypt='Sm@hTe$t';

  //prevent timeout from waiting event loop
  context.callbackWaitsForEmptyEventLoop = false;
  var userInfo = event.queryStringParameters.userId;
  console.log("userInfo="+JSON.stringify(userInfo));
 
  var encryptedUserId = String(decodeURIComponent(userInfo));
  var bytes = AES.decrypt(encryptedUserId, secretKeyToDecrypt);
  var userId = bytes.toString(Utf8);

  console.log("Decrypted userId=" + userId);

  //let tableName = process.env.tableName+"monthName";
  pool.getConnection(function(err, connection) {
  var updateUserQuery='select date,score from smahtest_score_Oct where user_id=?';

  //let userInfoValues=[[userId]];
    // Use the connection
    connection.query(updateUserQuery,userId, function (error, results, fields) {
      console.log("Query execution Error="+error);
      if(results.affectedRows==0) {
          console.log("User Doesn't Exist");
          responseMsg = JSON.stringify({
            result: "error",
            msg: "User does not exist, Please log in and try again!"
          })
          returnResponse(responseMsg,null,callback);
      }
      else {
      connection.release();
      if (error)
        callback(error);
      else {
             console.log("result ="+JSON.stringify(results));
             var userScore=JSON.stringify(results);
            responseMsg = JSON.stringify({
                  result: "success",
                  msg: userScore
              })
              returnResponse(responseMsg,null,callback);
            }
          }
    });
  });
};

function returnResponse(responseMsg,name,callback) {
  let response;
  try {
      response = {
          'statusCode': 200,
          headers: {
            "Access-Control-Allow-Headers" : "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
          },
          'body': responseMsg
      }
  } catch (err) {
      console.log("error occured="+err);
      return err;
  }
  callback(null,response);
}
