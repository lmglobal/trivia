'use strict';
var mysql = require('mysql');
exports.createUser =  (event, context, callback) => {
  /* var pool  = mysql.createPool({
    host     : '192.168.43.105',
    user     : 'sriAWS',
    password : 'admin',
    database : 'smahtest'
  });*/
  
 var pool  = mysql.createPool({
    host     : 'smahtest.c1gggoku9c0l.us-east-1.rds.amazonaws.com',
    user     : 'admin',
    password : 'Smahtest20$',
    database : 'smahtest'
  });
  var AES = require("crypto-js/aes");
  var secretKeyToEncrypt='Sm@hTe$t';

  //prevent timeout from waiting event loop
  context.callbackWaitsForEmptyEventLoop = false;
  var userInfo=JSON.parse(event.body);
  console.log("userInfo="+JSON.stringify(userInfo));
  pool.getConnection(function(err, connection) {
  var insertUserQuery="INSERT IGNORE INTO smahtest_users (email, name, image_url,source,adSource,created_dttm,last_login_dttm) VALUES ?";
  var currentDate=new Date();

  let userInfoValues=[[userInfo.userEmail,userInfo.userName,userInfo.userImageURL,userInfo.loggedInSource,userInfo.adSource,currentDate,currentDate]];
    // Use the connection
    connection.query(insertUserQuery,[userInfoValues], function (error, results, fields) {
      console.log("results="+JSON.stringify(results));
      if(results.affectedRows==0) {
          let existingUserEmail=[[userInfo.userEmail]];
          connection.query("SELECT id,name from smahtest_users where email=?",[existingUserEmail], function (error, results, fields) {
              connection.release();
              if (error)
                callback(error);
              else {
                var insertId= String(results[0].id);
                console.log("Insert id="+insertId);
                var ciphertext = AES.encrypt(insertId, secretKeyToEncrypt).toString();
                returnResponse(ciphertext,results[0].name,callback);
              }
          });
      }
      else {
      connection.release();
      if (error)
        callback(error);
      else {
              var insertId= String(results.insertId);
              console.log("Insert id="+insertId);
              var ciphertext = AES.encrypt(insertId, secretKeyToEncrypt).toString()
              returnResponse(ciphertext,null,callback);
            }
          }
    });
  });
};

function returnResponse(userId,name,callback) {
  let response;
  try {
      response = {
          'statusCode': 200,
          headers: {
            "Access-Control-Allow-Headers" : "*",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
          },
          'body': JSON.stringify({
              userId: userId,
              name: name,
          })
      }
  } catch (err) {
      console.log("error occured="+err);
      return err;
  }
  callback(null,response);
}
