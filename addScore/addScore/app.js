'use strict';

const util = require('util');
var mysql = require('mysql');

var Redis = require("redis");
var redisHost = 'redis-14115.c83.us-east-1-2.ec2.cloud.redislabs.com';
var redisPort = 14115;
var redisAuth = 'kVMz3M1yLutr4wnCkRK1AELvJBPsZRlo';

var Bluebird = require('bluebird')
Bluebird.promisifyAll(Redis.RedisClient.prototype)
Bluebird.promisifyAll(Redis.Multi.prototype)

var AES = require("crypto-js/aes");
var Utf8 = require("crypto-js/enc-utf8");
var secretKeyToDecrypt = 'Sm@hTe$t';

let responseMsg;

exports.addScore = async (event, context, callback) => {
    //prevent timeout from waiting event loop
    context.callbackWaitsForEmptyEventLoop = false;

     var pool  = mysql.createPool({
      host     : 'smahtest.c1gggoku9c0l.us-east-1.rds.amazonaws.com',
      user     : 'admin',
      password : 'Smahtest20$',
      database : 'smahtest'
    });

   
   /* var pool  = mysql.createPool({
    host     : '192.168.1.7',
    user     : 'sriAWS',
    password : 'admin',
    database : 'smahtest'
  });*/ 

    var scoreInfo = JSON.parse(event.body);
    console.log("scoreInfo=" + JSON.stringify(scoreInfo));

    var client = Redis.createClient({
        port: redisPort,
        host: redisHost
    });



    client.auth(redisAuth, function(err, response) {});
  
    var encryptedUserId = String(scoreInfo.userId);
    var bytes = AES.decrypt(encryptedUserId, secretKeyToDecrypt);
    var userId = bytes.toString(Utf8);

    console.log("Decrypted userId=" + userId);

    var currentDate = new Date().toISOString().slice(0, 10);
    /* Comment above and use this for testing
    var currentDate = scoreInfo.date;
    */
    var currentMonth = new Date().toLocaleString('default', {
        month: 'short'
    });
    

    if(null!=scoreInfo.oldAttemptDate){
      var encryptedDate = String(scoreInfo.oldAttemptDate);
      var bytes = AES.decrypt(encryptedDate, secretKeyToDecrypt);
      var oldDate = bytes.toString(Utf8);
      console.log("oldDate=" +oldDate);
      currentDate=oldDate;
      var date = new Date(currentDate);
      currentMonth=date.toLocaleString("en-us", { month: "short" });
      console.log("Updating for month="+currentMonth);
    }

    let tableName = process.env.tableName + currentMonth;

    var insertUserQuery = "INSERT INTO " + tableName + "(user_id,date,score,ip) VALUES ?";
    let scoreInfoValues = [
        [userId, currentDate, scoreInfo.score,scoreInfo.ip]
    ];
  
console.log("after pool.get connection userIp="+scoreInfo.ip);
    await (async () => {
        try {
          console.log("inside async await");

                  var existingUserScore = await insertScore(pool, insertUserQuery, scoreInfoValues,callback);

                  console.log("after insert query");
                  
                    //Add to Redis Cache
                    let uniqueScoreTable = process.env.uniqueScoreTable + currentMonth;
                    //Increment Existing Score - smahtest_score_Jul

                    var redisUserId = userId + "_" + scoreInfo.userName;

                    console.log("Starting with Redis uniqueScoreTable=" + uniqueScoreTable + " redisUserId=" + redisUserId);

                    var existingUserScore = await getExistingScore(tableName, redisUserId, client);


                    await addToUserScore(tableName, scoreInfo.score, redisUserId, client);

                    //Add Unique score to hash if not present - smahtest_unique_score_Jul

                    var uniqueScore = scoreInfo.score;
                    if (null != existingUserScore) {
                        uniqueScore = parseInt(uniqueScore) + parseInt(existingUserScore);
                        var numberOfUsers = await getUserCountWithSameScore(existingUserScore, tableName, client);

                        if (!numberOfUsers >= 1)
                            await removeExistingFromUniqueScore(uniqueScoreTable, existingUserScore, client);
                    }


                    await addToUniqueScore(uniqueScoreTable, uniqueScore, client);



                    var top3Scores = await getTop3Scores(uniqueScoreTable, client);


                    client.quit();
                    responseMsg = JSON.stringify({
                        result: "success",
                        msg: "Congrats! Score updated to your profile!"
                    })
                    returnResponse(responseMsg, callback);
        } catch (err) {
            console.log("error=" + err);
            console.log("error=" + err.code);
            if (err.code == "ER_DUP_ENTRY") {
                //Date entry already exists for this user
                responseMsg = JSON.stringify({
                    result: "error",
                    msg: "Seems like you have taken up the quiz already!! Please come back tomorrow!!"
                })
                returnResponse(responseMsg, callback)
            } else if (err.code == "ER_NO_REFERENCED_ROW_2") {
                //User id doesn't exist
                responseMsg = JSON.stringify({
                    result: "error",
                    msg: "User does not exist, Please log in and try again!"
                })
                returnResponse(responseMsg, callback)
            }
        }
    })()

};


async function insertScore(pool, insertUserQuery, scoreInfoValues,callback) {
  return new Promise(function(resolve, reject) { 
    
    pool.getConnection( function(err, connection) {
      console.log("Using pool connetionnnnnn");
          
      console.log("err=" + err);
      console.log("connection=" + connection);
      connection.query(insertUserQuery, [scoreInfoValues], async function(error, results, fields) {
          console.log("results=" + results);
          console.log("error=" + error);
    if(null!=error) {
      connection.release();
    if (error.code == "ER_DUP_ENTRY") {
        //Date entry already exists for this user
        responseMsg = JSON.stringify({
            result: "error",
            msg: "Seems like you have taken up the quiz already!! Please come back tomorrow!!"
        })
        returnResponse(responseMsg, callback)
    } else if (err.code == "ER_NO_REFERENCED_ROW_2") {
        //User id doesn't exist
        responseMsg = JSON.stringify({
            result: "error",
            msg: "User does not exist, Please log in and try again!"
        })
        returnResponse(responseMsg, callback)
    }
  }
    else {
      connection.release();
      resolve(true);
    }
      });
    });
  });
  }


async function getExistingScore(userToScoreHash, userId, client) {
    return await client.zscoreAsync(userToScoreHash, userId);

}

async function addToUserScore(userToScoreHash, score, userId, client) {
    return await client.zincrbyAsync(userToScoreHash, score, userId).then(function(existingUserScore) {
        return existingUserScore;
    });
}

async function getUserCountWithSameScore(score, userToScoreHashName, client) {
    return await client.zcountAsync(userToScoreHashName, score, score).then(function(numberOfUser) {
        return numberOfUser;
    });
}

async function removeExistingFromUniqueScore(uniqueScoreTable, existingUserScore, client) {
    console.log("inside removeExistingFromUniqueScore");
    return await client.zremAsync(uniqueScoreTable, existingUserScore);

}

async function addToUniqueScore(uniqueScoreTable, score, client) {
    console.log("inside addToUniqueScore");
    return await client.zadd(uniqueScoreTable, score, score);
}

async function getTop3Scores(uniqueScoreTable, client) {
    console.log("inside getTop3Scores");
    return await client.zrevrangeAsync(uniqueScoreTable, 0, 2).then(function(top3Scores) {
        return top3Scores;
    });
}

function returnResponse(responseMsg, callback) {
    let response;
    try {
        response = {
            'statusCode': 200,
            headers: {
                "Access-Control-Allow-Headers": "*",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
            },
            'body': responseMsg
        }
        callback(null, response);
    } catch (err) {
        console.log(err);
        return err;
    }
    callback(null, response);
}