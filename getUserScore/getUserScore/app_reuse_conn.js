'use strict';
var mysql = require('mysql');
var pool = mysql.createPool({
  host: '192.168.1.8',
  user: 'sriAWS',
  password: 'admin',
  database: 'smahtest'
});
var client ;
if (typeof client === 'undefined') {
  console.log("client is undefined");
  // Connect to the MySQL database
  var client = mysql.createConnection({
    host: '192.168.1.8',
    user: 'sriAWS',
    password: 'admin',
    database: 'smahtest'
  }); 
  client.connect()
}

let response;
let responseMsg;

exports.getUserScore = (event, context, callback) => {
  //prevent timeout from waiting event loop
  context.callbackWaitsForEmptyEventLoop = false;
   /* Comment above and use this for testing
  var currentDate = scoreInfo.date;
  */
  let userId = [[event.queryStringParameters.userId]];
  
  var currentMonth = new Date().toLocaleString('default', {
    month: 'short'
  });

  let tableName = process.env.tableName + currentMonth;
  
  //console.log("tableName="+tableName+" userId="+userId);
  // Use the connection
   client.query("SELECT SUM(SCORE) as totalScore from "+tableName+" where user_id=?",[userId], function(error, results, fields) {
    
     // console.log("results="+results[0].totalScore);
      if (error) {
        responseMsg = JSON.stringify({
          result: "error",
          errorMsg: error
        })
        returnResponse(responseMsg, callback)
      }
      else {
          //Success Scenario
          responseMsg = JSON.stringify({
            result: "success",
            totalScore: (results[0].totalScore!=null)?results[0].totalScore:0
          })
          returnResponse(responseMsg, callback);
        }


    });
  
};

function returnResponse(responseMsg, callback) {
  let response;
  try {
    response = {
      'statusCode': 200,
      headers: {
        "Access-Control-Allow-Headers": "*",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
      },
      'body': responseMsg
    }
    callback(null, response);
  } catch (err) {
    console.log(err);
    return err;
  }
  callback(null, response);
}
