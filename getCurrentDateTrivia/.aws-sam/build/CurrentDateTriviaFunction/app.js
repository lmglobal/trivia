let response;
const tableName = "TR_LEAGUE_QUESTIONS";
var AWS = require("aws-sdk");
S3  = new AWS.S3();

var docClient = new AWS.DynamoDB.DocumentClient();

var fs = require('fs');
var parse = require('csv-parse');
var async = require('async');
var AES = require("crypto-js/aes");

exports.getCurrentDateTrivia = async (event, context) => {
    var currentDate = new Date().toISOString().slice(0, 10);
	console.log("Retrieving Questions for="+ currentDate);
	 var params = {
		TableName : tableName,
		Key: { date: currentDate },
	  };
	  const data = await docClient.get(params).promise();
	  const qlist = data.Item.qlist;
	  //Remove Answer from Response
	  //let result = qlist.map(({answer, ...rest}) => rest);
	  
	  
	  //Save to S3
	   console.log("Writing json to s3" );
	  
	   S3.putObject({
         Bucket: 'triviareadquestions',
         Key: 'currentDate.json',
         Body: JSON.stringify(qlist)
		})
	  .promise()
         .then( () => console.log( 'UPLOAD SUCCESS' ) )
         .catch( e => {
            console.error( 'ERROR', e );
            callback( e );
         } );

	  const response = {
		statusCode: 200,
		body: JSON.stringify(qlist)
	  };
	  return response;
};
