let response;
const tableName = "TR_LEAGUE_QUESTIONS";
var AWS = require("aws-sdk");
S3  = new AWS.S3();

var docClient = new AWS.DynamoDB.DocumentClient();


exports.getCurrentDateTrivia = async (event, context) => {
    var currentDate = new Date();
    currentDate.setDate(currentDate.getDate());  
    
    var tomorrowFormatted = new Date(currentDate).toISOString().slice(0, 10);
    
    
    //tomorrowFormatted='2021-05-03';
	
	
	
	console.log("Retrieving Questions for tomorrow="+ tomorrowFormatted);
	 var params = {
		TableName : tableName,
		Key: { date: tomorrowFormatted },
	  };
	  console.log("params="+JSON.stringify(params));
	  const data = await docClient.get(params).promise();
	  console.log("data="+JSON.stringify(data));
	  const qlist = data.Item.qlist;
	  //Remove Answer from Response
	  //let result = qlist.map(({answer, ...rest}) => rest);
	  
	  
	  //Save to S3
	   console.log("Writing json to s3" );
	   var fileName=tomorrowFormatted+'.json';
	   
	   try {
        const s3Response = await  S3.putObject( {
         Bucket: 'smahtesttodayquiz',
         Key: fileName,
         Body:JSON.stringify(qlist),
         ACL:'public-read'
    	} ).promise();
	   console.log("Upload Successful fileUploaded="+fileName);
       }catch (e) {
            console.error( "Error Occured when trying Write Questions to S3 Exception="+e );
         } 
	   
	   
	   
	  const response = {
		statusCode: 200,
		body: JSON.stringify(qlist)
	  };
	  return response;
};
