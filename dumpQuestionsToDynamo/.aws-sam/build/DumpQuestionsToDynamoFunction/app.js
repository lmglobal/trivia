let response;
const tableName = "TR_LEAGUE_QUESTIONS";
var AWS = require("aws-sdk");
S3  = new AWS.S3();


var parse = require('csv-parse');
var async = require('async');
var AES = require("crypto-js/aes");


var dynamDB_TableName='TR_LEAGUE_QUESTIONS';
var csvfilePathWithName='https://smahtestquestiondump.s3.amazonaws.com/TR_LEAGUE_QUESTIONS.csv';
var secretKeyToEncryptAnswer='Sm@hTe$t';

var documentClient = new AWS.DynamoDB.DocumentClient({region: 'us-east-1'});


exports.dumpQuestionsToDynamo = async (event,context, callback) => {
	console.log("entering");
	context.callbackWaitsForEmptyEventLoop = false;
	await test();	
};


async function test() {
	await new Promise((resolve,reject) => { 
	try{
	const params = {
	  Bucket: 'smahtestquestiondump',
	  Key: 'TR_LEAGUE_QUESTIONS.csv'
	};
	 const stream = S3.getObject(params).createReadStream();
	rs = S3.getObject(params).createReadStream();
    parser = parse({
        columns : true,
        delimiter : ','
    }, function(err, data) {
				
	console.log("Total number of records="+data.length);
	var PutRequest = new Array();
    
    //Iterate for each item
	for(i=0;i<data.length;i++) {
	var item= 
	{
		PutRequest:
	{
		Item:
		{
			date: data[i].date,
			"qlist": [
			{"question" :  data[i].q1,
			 "options" : [data[i].q1option1, data[i].q1option2 ,data[i].q1option3,data[i].q1option4],
			 "answer" :  AES.encrypt(data[i].q1answer, secretKeyToEncryptAnswer).toString()
			},
			{
			"question" :  data[i].q2,
			"options" : [data[i].q2option1,data[i].q2option2 ,data[i].q2option3,data[i].q2option4],
			"answer" :  AES.encrypt(data[i].q2answer, secretKeyToEncryptAnswer).toString()
			},
			{"question" : data[i].q3,
			 "options" : [data[i].q3option1,data[i].q3option2 ,data[i].q3option3,data[i].q3option4],
			 "answer" :  AES.encrypt(data[i].q3answer, secretKeyToEncryptAnswer).toString()
			},
			{
			"question" :  data[i].q4,
			"options" : [data[i].q4option1,data[i].q4option2 ,data[i].q4option3,data[i].q4option4],
			"answer" :  AES.encrypt(data[i].q4answer, secretKeyToEncryptAnswer).toString()
			},
			{
			"question" :  data[i].q5,
			"options" : [data[i].q5option1,data[i].q5option2 ,data[i].q5option3,data[i].q5option4],
			"answer" : AES.encrypt(data[i].q5answer, secretKeyToEncryptAnswer).toString()
			},
			
			]
		}
	}
	}
	PutRequest.push(item);			
}

var params = {
  RequestItems: {
    'TR_LEAGUE_QUESTIONS':       
       PutRequest
  }
};

console.log("params="+JSON.stringify(params));

documentClient.batchWrite(params, function(err, data) {
  if (err) console.log("erorr occureddd="+err);
		  else {
			  console.log(data);
			  resolve();
		  }
});

});
         rs.pipe(parser);
	}
	catch(errrr) {
		console.log("errrr="+errrr);
	}
 });
}