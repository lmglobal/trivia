'use strict';
var mysql = require('mysql');
var AES = require("crypto-js/aes");
var Utf8 = require("crypto-js/enc-utf8");
var secretKeyToDecrypt = 'Sm@hTe$t';
let responseText;
exports.fcmSubscribe = (event, context, callback) => {

   var pool = mysql.createPool({
        host: 'smahtest.c1gggoku9c0l.us-east-1.rds.amazonaws.com',
        user: 'admin',
        password: 'Smahtest20$',
        database: 'smahtest'
    });
/*
var pool = mysql.createPool({
    host: '192.168.1.7',
    user: 'sriAWS',
    password: 'admin',
    database: 'smahtest'
  });*/
    //prevent timeout from waiting event loop
    context.callbackWaitsForEmptyEventLoop = false;
    var tokenInfo = JSON.parse(event.body);
    console.log("tokenInfo=" + JSON.stringify(tokenInfo));
    pool.getConnection(function (err, connection) {

        var insertUserQuery = "INSERT INTO smahtest_users_fcm (user_Id, fcm_key, created_date) VALUES ?";
        var currentDate = new Date();
        var encryptedUserId = String(tokenInfo.userId);

        console.log("encryptedUserId="+encryptedUserId);
        var bytes  = AES.decrypt(encryptedUserId, secretKeyToDecrypt);
        var decryptedUserID = bytes.toString(Utf8);
        console.log("decryptedUserID=" + decryptedUserID);
        let tokenInfoValues = [
            [decryptedUserID, tokenInfo.tokenId, currentDate]
        ];
        // Use the connection
        connection.query(insertUserQuery, [tokenInfoValues], function (error, results, fields) {
            console.log("results=" + JSON.stringify(results));
                connection.release();
                if (error) {
                    console.log("Error="+error+" decryptedUserID="+decryptedUserID);
                    responseText="There was an Error while enrolling you to Push Service, Please try again later!";
                    returnResponse("error",responseText ,callback);
                }
                else {
                    var insertId = String(results.insertId);
                    if(null!=insertId) {
                        responseText="You have successfully enrolled to our Push Reminder Service";
                        returnResponse("success",responseText,callback);
                    }
                    else {
                        console.log("Insert Id is null decryptedUserID="+decryptedUserID);
                        responseText="There was an Error while enrolling you to Push Service, Please try again later!";
                        returnResponse("error",responseText ,callback);
                    }
                }
        });
    });
};

function returnResponse(result,responseText,callback) {
    let response;
    try {
        response = {
            'statusCode': 200,
            headers: {
                "Access-Control-Allow-Headers": "*",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
            },
            'body': JSON.stringify({
                result: result,
                msg: responseText,
            })
        }
    } catch (err) {
        console.log("error occured=" + err);
        return err;
    }
    callback(null, response);
}