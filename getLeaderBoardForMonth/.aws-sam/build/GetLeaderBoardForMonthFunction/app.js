'use strict';
var Redis = require("redis");
var redisHost = 'redis-14115.c83.us-east-1-2.ec2.cloud.redislabs.com';
var redisPort = 14115;
var redisAuth = 'kVMz3M1yLutr4wnCkRK1AELvJBPsZRlo';
var Bluebird = require('bluebird')
Bluebird.promisifyAll(Redis.RedisClient.prototype)
Bluebird.promisifyAll(Redis.Multi.prototype)
var AES = require("crypto-js/aes");
var Utf8 = require("crypto-js/enc-utf8");
var secretKeyToDecrypt='Sm@hTe$t';
let response;
exports.getLeaderBoardForMonth =  async(event, context, callback) => {
let leaderList = [];

var currentMonth = new Date().toLocaleString('default', {
month: 'short'
});
console.log("before setting currentMonth="+currentMonth);
var month = event.queryStringParameters.month;
console.log("month param="+month);
if(month!=null){
currentMonth=month.substring(0,3);
console.log("currentMonth="+currentMonth);
}
let userToScoreHashName = process.env.userToScoreHashName + currentMonth;
let uniqueScoreHashName=  process.env.uniqueScoreHashName + currentMonth;

var client = Redis.createClient ({
port : redisPort,
host : redisHost
});

client.auth(redisAuth, function(err, response){
});
//console.log("currentMonth="+currentMonth);
//console.log("userToScoreHashName="+userToScoreHashName);
//console.log("uniqueScoreHashName="+uniqueScoreHashName);
var top10Scores = await getTop10Scores(uniqueScoreHashName,client);     
console.log("top10Scores="+top10Scores);
if(null==top10Scores) {
console.log("No top scores=");
leaderList.push({"monthlyScore": "NA","rank":"NA"});
leaderList.push({"score": 0,"numberOfUsers": 0,"userIds":0});
client.quit();
returnResponse(leaderList,callback);
}

//Populate Current User Rank
var userId = event.queryStringParameters.userId;
console.log("Encrypted userId="+userId);
if(userId=="null" || userId==null) {
console.log("userId is null");
leaderList.push({"monthlyScore": "NA","rank":"NA"});
}
else {
var userIdWoName = new Array();

userIdWoName = userId.split("__");
if(userIdWoName.length==2){
var encryptedUserId=String(decodeURIComponent(userIdWoName[0]));
var bytes  = AES.decrypt(encryptedUserId, secretKeyToDecrypt);
var decryptedId = bytes.toString(Utf8);
console.log("DecryptedId userId="+decryptedId);
decryptedId=decryptedId+'_'+userIdWoName[1];
var monthlyScore=await getExistingScore(userToScoreHashName,decryptedId,client);
console.log("monthlyScore="+monthlyScore);
if(monthlyScore!=null) {
var currentUserRank=await getRankForScore(uniqueScoreHashName,monthlyScore,client);
console.log("currentUserRank="+currentUserRank);
leaderList.push({"monthlyScore": monthlyScore,"rank":currentUserRank+1});
}
else {
leaderList.push({"monthlyScore": "NA","rank":"NA"});
}
}
else {
leaderList.push({"monthlyScore": "NA","rank":"NA"});
}
} 


for (const score of top10Scores) {
var numberOfUsers=await getUserCountWithSameScore(score,userToScoreHashName,client);
console.log("numberOfUsers="+numberOfUsers);
var userIds=await getTop5Users(score,userToScoreHashName,client);
leaderList.push({"score": score,"numberOfUsers": numberOfUsers>=6?numberOfUsers-5:0,"userIds":userIds});

}

returnResponse(leaderList,callback,client);

};

async function getExistingScore(userToScoreHash,userId,client) {
return await client.zscoreAsync(userToScoreHash,userId);

}

async function getRankForScore(uniqueScoreHashName,score,client) {
return await client.zrevrankAsync(uniqueScoreHashName,score);

}

async function getTop10Scores(uniqueScoreHashName,client) {
return await client.zrevrangeAsync(uniqueScoreHashName, 0, 9).then(function(top10Scores) {
return top10Scores;
});		    
}

async function getUserCountWithSameScore(score,userToScoreHashName,client) {
return await client.zcountAsync(userToScoreHashName,score,score).then(function (numberOfUser) {
return numberOfUser;
});
}

async function getTop5Users(score,userToScoreHashName,client) {
return await client.zrangebyscoreAsync(userToScoreHashName,score,score,"limit",0,5).then(function(userIds) {
return userIds;
});		    
}

function returnResponse(leaderList,callback,client) {
let response;
try {
response = {
'statusCode': 200,
headers: {
"Access-Control-Allow-Headers" : "*",
"Access-Control-Allow-Origin": "*",
"Access-Control-Allow-Methods": "OPTIONS,POST,GET"
},
'body': JSON.stringify(leaderList)
}
console.log("leaderList="+JSON.stringify(leaderList));
client.quit();
callback(null,response);
} catch (err) {
console.log(err);
return err;
}
callback(null,response);
}
